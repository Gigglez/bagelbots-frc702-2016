package org.usfirst.frc.team702.robot;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;

public class AnglePIDController extends PIDController {

	public AnglePIDController(double Kp, double Ki, double Kd, PIDSource source, PIDOutput output) {
		super(Kp, Ki, Kd, source, output);
		// TODO Auto-generated constructor stub
	}

	public AnglePIDController(double Kp, double Ki, double Kd, PIDSource source, PIDOutput output, double period) {
		super(Kp, Ki, Kd, source, output, period);
		// TODO Auto-generated constructor stub
	}

	public AnglePIDController(double Kp, double Ki, double Kd, double Kf, PIDSource source, PIDOutput output) {
		super(Kp, Ki, Kd, Kf, source, output);
		// TODO Auto-generated constructor stub
	}

	public AnglePIDController(double Kp, double Ki, double Kd, double Kf, PIDSource source, PIDOutput output,
			double period) {
		super(Kp, Ki, Kd, Kf, source, output, period);
		// TODO Auto-generated constructor stub
	}
	
	/**
   * Returns the current difference of the input from the setpoint
   *$
   * @return the current error
   */
  public synchronized double getError() {
    // return m_error;
	  
    return RAWDrive.normalizeAngle( (float)(getSetpoint() - m_pidInput.pidGet()) );
  }

}
